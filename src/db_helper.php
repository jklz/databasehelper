<?php

namespace JKLZ\Helper;

class DBModel extends \Illuminate\Database\Eloquent\Model {
	use \JKLZ\Helper\DBHelper;
	protected $encryptable = [];
	protected $jsonable = [];
	
	public function count($column = '*') {
		$eloBuilder = $this->newQuery();
		$builder = $eloBuilder->getQuery();
		return $builder->count('*');
	}
}

trait DBHelper{

	protected $crypt_handler;
	protected $crypt_key;
    public function isencryptable($key){
        return in_array($key, $this->encryptable);
    }
    public function isjsonable($key){
        return in_array($key, $this->jsonable);
    }
	
	protected function _cryptHelper(){
		if(!$this->crypt_handler){
			//first we get key from .env
			$this->crypt_key = (array_key_exists("CRYPT.KEY",$_ENV) ? $_ENV["CRYPT.KEY"]:"");
			//now we check the key is correct or generate one by md5 server address
			$this->crypt_key = (strlen($this->crypt_key) == 32 ? $this->crypt_key : md5($_SERVER['SERVER_ADDR']));
			$this->crypt_handler = new \Illuminate\Encryption\Encrypter($this->crypt_key, 'AES-256-CBC');
		}
	}

    protected function decryptAttribute($value){
		$this->_cryptHelper();
		if(strlen($value) > 0){
			$value = $this->crypt_handler->decrypt($value);
		}
        return $value;
    }


    protected function encryptAttribute($value){
		$this->_cryptHelper();
        $value = $this->crypt_handler->encrypt($value);
        return $value;
    }

    public function getAttribute($key){
        $value = parent::getAttribute($key);
        //first decrypt
		if ($this->isencryptable($key)) {
            $value = $this->decryptAttribute($value);
        }
		//now process for json
		if ($this->isjsonable($key) && is_string($value)) {
            $value = json_decode($value, true);
        }elseif ($this->isjsonable($key) && is_null($value)){
			$value = array();
		}
        return $value;
    }

    public function setAttribute($key, $value){
        //process to json
		if ($this->isjsonable($key)) {
            $value = json_encode($value);
        }
		//process to encrypt
		if ($this->isencryptable($key)) {
            $value = $this->encryptAttribute($value);
        }

        return parent::setAttribute($key, $value);
    }

    public function getArrayableAttributes(){
        $attributes = parent::getArrayableAttributes();

        foreach ($attributes as $key => $attribute) {
			$value = $attribute;
            if ($this->isencryptable($key)) {
                $value = $this->decryptAttribute($value);
            }
			if ($this->isjsonable($key) && is_string($value)) {
                $value = json_decode($value, true);
            }elseif ($this->isjsonable($key) && is_null($value)) {
                $value = array();
            }
			$attributes[$key] = $value;
        }

        return $attributes;
    }
}